package br.com.dbccompany.userslog.Service;

import br.com.dbccompany.userslog.DTO.RoleDTO;
import br.com.dbccompany.userslog.Entity.RoleEntity;
import br.com.dbccompany.userslog.Entity.UsuarioEntity;
import br.com.dbccompany.userslog.Repository.RoleRepository;
import br.com.dbccompany.userslog.UserslogApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    private Logger logger = LoggerFactory.getLogger(UserslogApplication.class);

    public List findAll() {
        return (List) roleRepository.findAll();
    }


    public RoleEntity save(RoleDTO roleDTO) {
        logger.warn("Se tiver campos faltando não será possível salvar a Entidade");

        try {
            RoleEntity roleEntity = roleDTO.convert();
            return roleRepository.save(roleEntity);
        } catch (Exception e) {
            logger.error("Erro ao salvar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public RoleEntity finById(Long id) {

        try {
            return roleRepository.findById(id).get();
        } catch (Exception e) {
            logger.error("Erro ao editar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public void delete(Long id) {
        try {
            RoleEntity entity = roleRepository.findById(id).get();
            roleRepository.delete(entity);
        } catch (Exception e) {
            logger.error("Erro ao remover entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
