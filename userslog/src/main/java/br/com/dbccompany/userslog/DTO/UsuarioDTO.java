package br.com.dbccompany.userslog.DTO;

import br.com.dbccompany.userslog.Entity.RoleEntity;
import br.com.dbccompany.userslog.Entity.UsuarioEntity;

import java.util.List;

public class UsuarioDTO {

    private Long id;
    private String nome;
    private String email;
    private String login;
    private String senha;
    private List<RoleEntity> roles;

    public UsuarioDTO() {}

    public UsuarioDTO(UsuarioEntity usuario) {
        this.id = usuario.getId();
        this.nome = usuario.getNome();
        this.email = usuario.getEmail();
        this.login = usuario.getLogin();
        this.senha = usuario.getSenha();
        this.roles = usuario.getRoles();
    }

    public UsuarioEntity convert() {
        UsuarioEntity usuarioEntity = new UsuarioEntity();
        usuarioEntity.setId(this.id);
        usuarioEntity.setEmail(this.email);
        usuarioEntity.setLogin(this.login);
        usuarioEntity.setNome(this.nome);
        usuarioEntity.setSenha(this.senha);
        usuarioEntity.setRoles(this.roles);
        return usuarioEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleEntity> roles) {
        this.roles = roles;
    }
}
