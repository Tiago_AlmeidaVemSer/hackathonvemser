package br.com.dbccompany.userslog.Repository;

import br.com.dbccompany.userslog.Entity.RoleEntity;
import br.com.dbccompany.userslog.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Long> {
    UsuarioEntity findByLoginAndSenha(String login, String senha);
    UsuarioEntity findByLogin(String login);
    UsuarioEntity findByRoles(RoleEntity role);
}
