package br.com.dbccompany.userslog;

import br.com.dbccompany.userslog.Service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserslogApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserslogApplication.class, args);
	}

}
