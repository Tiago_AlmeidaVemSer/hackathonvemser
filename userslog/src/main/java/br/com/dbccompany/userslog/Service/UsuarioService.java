package br.com.dbccompany.userslog.Service;

import br.com.dbccompany.userslog.DTO.UsuarioDTO;
import br.com.dbccompany.userslog.Entity.UsuarioEntity;
import br.com.dbccompany.userslog.Repository.UsuarioRepository;
import br.com.dbccompany.userslog.UserslogApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    private Logger logger = LoggerFactory.getLogger(UserslogApplication.class);

    @Transactional
    public List<UsuarioEntity> findAll() {
        try {
            return (List) usuarioRepository.findAll();
        }catch (Exception e) {
            logger.error("Erro ao remover entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional
    public UsuarioEntity findById(Long id) {
        try {
            return usuarioRepository.findById(id).get();
        }catch (Exception e) {
            logger.error("Erro ao remover entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public UsuarioEntity save(UsuarioDTO usuarioDTO) {
        try {
            UsuarioEntity usuarioEntity = usuarioDTO.convert();
            return usuarioRepository.save(usuarioEntity);
        }catch (Exception e) {
            logger.error("Erro ao remover entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public void delete(Long id) {

        try {
            usuarioRepository.deleteById(id);
        }catch (Exception e) {
            logger.error("Erro ao remover entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
