package br.com.dbccompany.userslog.Controller;

import br.com.dbccompany.userslog.DTO.RoleDTO;
import br.com.dbccompany.userslog.DTO.UsuarioDTO;
import br.com.dbccompany.userslog.Entity.RoleEntity;
import br.com.dbccompany.userslog.Entity.UsuarioEntity;
import br.com.dbccompany.userslog.Service.RoleService;
import br.com.dbccompany.userslog.Service.UsuarioService;
import br.com.dbccompany.userslog.UserslogApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/role")
@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    private Logger logger = LoggerFactory.getLogger(UserslogApplication.class);

    @RequestMapping(value="/novo", method = RequestMethod.POST)
    @ResponseBody
    public RoleEntity salvar(@RequestBody RoleDTO roleDTO){
        logger.info("Salvando nova permissão");
        return roleService.save(roleDTO);
    }

    @RequestMapping(value="/todos", method = RequestMethod.GET)
    @ResponseBody
    public List listRole(){
        logger.info("Listando permissões");
        return roleService.findAll();
    }

    @RequestMapping(value = "/ver/{id}", method = RequestMethod.GET)
    @ResponseBody
    public RoleEntity getOne(@PathVariable(value = "id") Long id){
        logger.info("Apresentano uma permissão");
        return roleService.finById(id);
    }


    @RequestMapping(value="/deleta/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public UsuarioEntity deleteUser(@PathVariable(value = "id") Long id){
        logger.info("Removendo permissão");
        roleService.delete(id);
        return new UsuarioEntity(id);
    }

}
