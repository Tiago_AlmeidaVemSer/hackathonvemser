package br.com.dbccompany.userslog.Controller;

import br.com.dbccompany.userslog.DTO.UsuarioDTO;
import br.com.dbccompany.userslog.Entity.UsuarioEntity;
import br.com.dbccompany.userslog.Service.UsuarioService;
import br.com.dbccompany.userslog.UserslogApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/usuario")
@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService userService;

    private Logger logger = LoggerFactory.getLogger(UserslogApplication.class);

    @RequestMapping(value="/novo", method = RequestMethod.POST)
    @ResponseBody
    public UsuarioEntity salvar(@RequestBody UsuarioDTO usuarioDTO){
        logger.info("Salvando novo usuario");
        return userService.save(usuarioDTO);
    }

    @RequestMapping(value="/todos", method = RequestMethod.GET)
    @ResponseBody
    public List<UsuarioEntity> listUser(){
        logger.info("Listando usuarios");
        return userService.findAll();
    }

    @RequestMapping(value = "/ver/{id}", method = RequestMethod.GET)
    @ResponseBody
    public UsuarioEntity getOne(@PathVariable(value = "id") Long id){
        logger.info("Aprensentando usuario");
        return userService.findById(id);
    }

    @RequestMapping(value="/login", method = RequestMethod.POST)
    @ResponseBody
    public UsuarioEntity logar(@RequestBody UsuarioDTO usuarioDTO){
        logger.info("Logando no sistema");
        return userService.save(usuarioDTO);
    }

    @RequestMapping(value="/deleta/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public UsuarioEntity deleteUser(@PathVariable(value = "id") Long id){
        logger.info("Removendo usuario");
        userService.delete(id);
        return new UsuarioEntity(id);
    }
    @RequestMapping(value = "/data/in")
    @ResponseBody
    public String dataIn() {
        logger.info("Entrando em data in");
        return "Somente Analistas aqui";
    }

    @RequestMapping(value = "/data/out")
    @ResponseBody
    public String dataOut() {
        logger.info("Entrando em data out");
        return "Somente Analistas e Consultores";
    }

}
