package br.com.dbccompany.userslog.Repository;

import br.com.dbccompany.userslog.Entity.RoleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends CrudRepository<RoleEntity, Long> {
    RoleEntity findByNome(String nome);
}
