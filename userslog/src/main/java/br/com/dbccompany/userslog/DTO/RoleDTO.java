package br.com.dbccompany.userslog.DTO;

import br.com.dbccompany.userslog.Entity.RoleEntity;
import br.com.dbccompany.userslog.Entity.UsuarioEntity;

import java.util.List;

public class RoleDTO {

    private Long id;
    private String nome;
    private List<UsuarioEntity> usuarios;

    public RoleDTO() {
    }

    public RoleDTO(RoleEntity roleEntity) {
        this.id = roleEntity.getId();
        this.nome = roleEntity.getNome();
        this.usuarios = roleEntity.getUsuarios();
    }

    public RoleEntity convert() {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(this.id);
        roleEntity.setNome(this.nome);
        roleEntity.setUsuarios(this.usuarios);
        return roleEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<UsuarioEntity> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioEntity> usuarios) {
        this.usuarios = usuarios;
    }
}
