package br.com.dbccompany.userslog.Repository;

import br.com.dbccompany.userslog.Entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository repository;

    @Test
    public void salvaUsuarioERetornaPorLogin() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Joao da Silva");
        usuario.setEmail("joao@dasilva.com");
        usuario.setLogin("joaosilva");
        usuario.setSenha("123456");
        repository.save(usuario);
        assertEquals(usuario.getNome(), repository.findByLogin("joaosilva").getNome());
        assertEquals(usuario.getEmail(), repository.findByLogin("joaosilva").getEmail());
        assertEquals(usuario.getLogin(), repository.findByLogin("joaosilva").getLogin());
        assertEquals(usuario.getSenha(), repository.findByLogin("joaosilva").getSenha());
    }

    @Test
    public void salvaUsuarioERetornaPorLoginESenha() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Joao da Silva");
        usuario.setEmail("joao@dasilva.com");
        usuario.setLogin("joaosilva");
        usuario.setSenha("123456");
        repository.save(usuario);
        assertEquals(usuario.getNome(), repository.findByLoginAndSenha("joaosilva","123456").getNome());
        assertEquals(usuario.getEmail(), repository.findByLoginAndSenha("joaosilva", "123456").getEmail());
        assertEquals(usuario.getLogin(), repository.findByLoginAndSenha("joaosilva", "123456").getLogin());
        assertEquals(usuario.getSenha(), repository.findByLoginAndSenha("joaosilva", "123456").getSenha());
    }

    @Test
    public void retornaNullSeUsuarioNaoForCriado() {
        String login = "joaosilva";
        String senha = "123456";
        assertNull(repository.findByLogin(login));
        assertNull(repository.findByLoginAndSenha(login, senha));
    }

    @Test
    public void salvaDoisUsuariosERetornaTodosPorLogin() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Joao da Silva");
        usuario.setEmail("joao@dasilva.com");
        usuario.setLogin("joaosilva");
        usuario.setSenha("123456");
        repository.save(usuario);

        UsuarioEntity usuarioEntity = new UsuarioEntity();
        usuarioEntity.setNome("Ze da Silva");
        usuarioEntity.setEmail("ze@dasilva.com");
        usuarioEntity.setLogin("zesilva");
        usuarioEntity.setSenha("654321");
        repository.save(usuarioEntity);

        List<UsuarioEntity> usuarios = (List<UsuarioEntity>) repository.findAll();

        assertEquals("joaosilva", repository.findByLogin("joaosilva").getLogin());
        assertEquals("zesilva", repository.findByLogin("zesilva").getLogin());
    }

}
