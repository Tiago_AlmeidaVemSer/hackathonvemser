package br.com.dbccompany.logs.hackathon.Config;

import br.com.dbccompany.logs.hackathon.Modelo.DadosDeEntrada;
import br.com.dbccompany.logs.hackathon.Util.HashMapIteratorUtil;
import br.com.dbccompany.logs.hackathon.Util.ParseArrayToValueUtil;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.util.HashMap;

@EnableBatchProcessing
@Configuration
public class BatchConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    String path = "/home/tiago/Data/in.dat";
    String pathSaida = "/home/tiago/Data/out.dat";

    public FlatFileItemWriter<DadosDeEntrada> writer() {
        FlatFileItemWriter<DadosDeEntrada> dadosWriter = new FlatFileItemWriter<>();
        dadosWriter.setResource(new FileSystemResource(pathSaida));
        dadosWriter.setAppendAllowed(true);
        dadosWriter.setLineAggregator(new DelimitedLineAggregator<DadosDeEntrada>(){
            {
                setDelimiter("ç");
                setFieldExtractor(new BeanWrapperFieldExtractor<DadosDeEntrada>(){
                    {
                        setNames(new String[]{
                                "dado1", "dado2", "dado3", "dado4"
                        });
                    }
                });
            }
        });
        return dadosWriter;
    }

    @Bean
    public FlatFileItemReader<DadosDeEntrada> reader() {
        Resource[] resources = null;
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        try{
            resources = patternResolver.getResources(path);
        }catch(Exception e){
            e.printStackTrace();
        }
        return new FlatFileItemReaderBuilder<DadosDeEntrada>()
                .name("dadosItemReader")
                .resource(new FileSystemResource(path))
                .delimited().delimiter("ç")
                .names(new String[]{"dado1", "dado2", "dado3", "dado4"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<DadosDeEntrada>() {{
                    setTargetType(DadosDeEntrada.class);
                }})
                .build();
    }


    @Bean
    public Job dadosJob(){
        return jobBuilderFactory
                .get("dadosJob")
                .start(dadosStep())
                .build();
    }

    @Bean
    public Step dadosStep(){
        return stepBuilderFactory
                .get("dadosStep")
                .<DadosDeEntrada, DadosDeEntrada>chunk(1)
                .reader(reader())
                .processor(new ItemProcessor<DadosDeEntrada, DadosDeEntrada>() {
                    private int qtdVendedor = 0;
                    private int qtdClientes = 0;
                    private double valorMaiorVenda = 0;
                    private String nomeFuncionario = "";
                    private HashMap<String, Double> vendasPorFuncionario = new HashMap<>();

                    @Override
                    public DadosDeEntrada process(DadosDeEntrada dadosDeEntradaItemReader) {
                        if(dadosDeEntradaItemReader.getDado1().equals("001")){
                            qtdVendedor++;
                        }

                        if(dadosDeEntradaItemReader.getDado1().equals("002")){
                            qtdClientes++;
                        }
                        if(dadosDeEntradaItemReader.getDado1().equals("003")){
                            double aux = ParseArrayToValueUtil.converteParaValor(dadosDeEntradaItemReader.getDado3());
                            if(aux > valorMaiorVenda){
                                valorMaiorVenda = aux;
                            }
                            if(vendasPorFuncionario.containsKey(dadosDeEntradaItemReader.getDado4())){
                                vendasPorFuncionario.put(dadosDeEntradaItemReader.getDado4(), vendasPorFuncionario.get(dadosDeEntradaItemReader.getDado4()+aux));
                            }else{
                                vendasPorFuncionario.put(dadosDeEntradaItemReader.getDado4(), aux);
                            }
                            nomeFuncionario = HashMapIteratorUtil.verificaMenorValorHashMap(vendasPorFuncionario);
                        }

                        final Integer id = dadosDeEntradaItemReader.getId();
                        final String dados1 = ""+qtdClientes;
                        final String dados2 = ""+qtdVendedor;
                        final String dados3 = ""+valorMaiorVenda;
                        final String dados4 = nomeFuncionario;

                        final DadosDeEntrada novoDadosDeEntrada = new DadosDeEntrada(id, dados1, dados2, dados3, dados4);

                        return novoDadosDeEntrada;
                    }
                })
                .writer(writer())
                .build();
    }

}
