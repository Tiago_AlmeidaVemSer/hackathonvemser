  **Projeto Hackathon** 
  
  **Vemser** 

## Resumo 

O repositório consiste de duas aplicações Java 8:
Uma para gerenciar o login dos usuários com suas permissões e outra para extrair informações de arquivos 
*dat ,* analisando-as e em seguida salvando em outro arquivo.

Para o programa hackathon os caminhos dos arquivos devem ser passados na classe BatchConfig.
*exemplo:*
```
String path = "/home/usuario/Data/in.dat";
String pathSaida = "/home/usuario/Data/out.dat";
```

**O arquivo convertido pelo programa hackathon apresenta, por linha, um sumário das operações da seguinte forma:**

- quantidade de clientes no arquivo de entrada
- quantidade de vendedor no arquivo de entrada
- id da venda mais cara
- o pior vendedor
- a ultima linha do arquivo de saida representa a analize completa dos dados

## Autores

* **Tiago Almeida, Tiago Falcon, Matheus Sheffer**


